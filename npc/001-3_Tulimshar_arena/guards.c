
001-3,77,37,0	script	[Phaet]#2	NPC125,{
	msqb(l("Phaet the Royal Guard"));
	mesq(l("Do you want to go back?"));
	next;
	
	menu
		l("Yes, please."), L_Sure,
		l("No, thank you."), -;
	mes(l("Have fun."));
	close;

L_Sure:
	msqb(l("Phaet the Royal Guard"));
	mesq(l("Ok."));
	next;
	
	warp "001-2", 25, 23;
	close;
}

001-3,69,37,0	script	[Aradin]	NPC126,{
	msqb(l("Aradin the Royal Guard"));
	mesql(l("Don't touch me..."));
	mesqr(l("You wouldn't enjoy the consequences."));
	close;
}
