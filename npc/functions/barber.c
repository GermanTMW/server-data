
function	script	Barber	{
	set @npcname$, getarg(0);
	msqb l(@npcname$);
	mesq l("What kind of look change do you want?");
	switch(select(l("Change my style"), l("Change my Color"), l("I am fine for now...")))
	{
		case 1:
			switch(select(l("Bald"),
				l("Flat ponytail"),
				l("Bowl cut"),
				l("Combed back"),
				l("Emo"),
				l("Mohawk"),
				l("Pompadour"),
				l("Center parting/Short and slick"),
				l("Long and slick"),
				l("Short and curly"),
				l("Pigtails"),
				l("Long and curly"),
				l("Parted"),
				l("Perky ponytail"),
				l("Wave"),
				l("Mane"),
				l("Bun"),
				l("Long and Clipped"),
				l("Fizzy"),
				l("Surprise me"),
				l("Nevermind...")))
			{
				if (@menu != 20)
				{
					set @style, @menu - 1;
					if (@style == 19) set @style, rand(19);
					setlook LOOK_HAIR, @style;
				}
				return;
			}
		case 2:
			switch(select(l("lightbrown"),
				l("green"),
				l("red"),
				l("lightpurple"),
				l("grey"),
				l("yellow"),
				l("blue"),
				l("brown"),
				l("lightblue"),
				l("darkpurple"),
				l("black"),
				l("shock white"),
				l("Surprise me"),
				l("Nevermind...")))
			{
				if (@menu != 14)
				{
					set @color, @menu - 1;
					if (@color == 12) set @color, rand(12);
					setlook LOOK_HAIR_COLOR, @color;
				}
				return;
			}

		default:
			return;
	}
	return;
}
