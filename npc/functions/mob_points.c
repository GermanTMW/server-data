
function	script	MobPoints	{
	if (@mobId < 1002) return;

	setarray @points,//				[0000]
		1,	// Maggot				[1002]
		2,	// Scorpion				[1003]
		20,	// Red Scorpion			[1004]
		10,	// Green Slime			[1005]
		30,	// Giant Maggot			[1006]
		15,	// Yellow Slime			[1007]
		25,	// Red Slime			[1008]
		45,	// Black Scorpion		[1009]
		50,	// Snake				[1010]
		4,	// Fire Goblin			[1011]
		55,	// Spider				[1012]
		23,	// Evil Mushroom		[1013]
		35,	// Flower				[1014]
		40,	// Santa Slime			[1015]
		15,	// Rudolph Slime		[1016]
		2,	// Bat					[1017]
		16,	// Pinkie				[1018]
		17,	// Shroom				[1019]
		14,	// Fluffy				[1020]
		25,	// Cave Snake			[1021]
		100,	// Jack-O			[1022]
		80,	// Fire Skull			[1023]
		80,	// Poison Skull			[1024]
		20,	// Stumpy				[1025]
		70,	// Mountain Snake		[1026]
		15,	// Easter Fluffy		[1027]
		40,	// Mouboo				[1028]
		0,	// Mauve Plant			[1029]
		0,	// Cobalt Plant			[1030]
		0,	// Gamboge Plant		[1031]
		0,	// Alizarin Plant		[1032]
		20,	// Sea Slime			[1033]
		75,	// Grass Snake			[1034]
		0,	// Silk Worm			[1035]
		120,	// Zombie			[1036]
		0,	// Clover Patch			[1037]
		2,	// Squirrel				[1038]
		0,	// Fire Lizard			[1039]
		80,	// Wisp					[1040]
		0,	// Snail				[1041]
		80,	// Spectre 				[1042]
		100,	// Skeleton 		[1043]
		100,	// Lady Skeleton	[1044]
		120,	// Fallen 			[1045]
		0, 	// Snake Lord 			[1046]
		80,	// Poltergeist 			[1047]
		0,	// Duck					[1048]
		0,	// Bird					[1049]
		0,	// LavaSlime			[1050]
		0,	// SnowFlake			[1051]
		0,	// GrimRipper			[1052]
		0,	// GFee					[1053]
		0,	// Dragon				[1054]
		0,	// Dog					[1055]
		0,	// SantaClaus			[1056]
		0,	// Reindeer				[1057]
		0,	// RFee					[1058]
		0,	// BFee					[1059]
		0,	// DunklerLord			[1060]
		0,	// Zentaur				[1061]
		2,	// Biene				[1062]
		3,	// Schmetterling		[1063]
		5,	// Hase					[1064]
		0,	// FeurigerDrache		[1065]
		0,	// Moggun				[1066]
		0,	// Terranite			[1067]
		4,	// IceGoblin			[1068]
		0,	// Reaper				[1069]
		0,	// Sense				[1070]
		0,	// Sehle				[1071]
		0,	// Schlidkroete			[1072]
		140,// Fafnir				[1073]
		0,	// Falke				[1074]
		// Add more here		
		0;	// END					[0000]


	// Make money too!
	if (rand(10) > 7) set Zeny, Zeny + (@points[@mobId - 1002]*8 + rand(@points[@mobId - 1002]*4))/10;

	if (MPQUEST == 1) set Mobpt, Mobpt + @points[@mobId - 1002];

	if ((@mobId == 1003) || (@mobId == 1004) || (@mobId == 1009))
		goto L_Good;

	// Attitude adjustment for the witch (can we refactor this to another function?  Not sure about max. recursion depth)

	set @value, 0;
	if (@mobId == 1028)
		set @value, 4;
	if (@mobId == 1020)
		set @value, 3;
	if (@mobId == 1038)
		set @value, 2;

	if (@value == 0)
		goto L_End;

	callfunc "QuestSagathaAnnoy";
	goto L_End;

L_Good:
	set @value, 1;
	callfunc "QuestSagathaHappy";
L_End:
        @value = 0;
}
