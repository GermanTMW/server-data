//mapflags
001-1	mapflag	mask	1
002-1	mapflag	mask	1
003-1	mapflag	mask	1
004-1	mapflag	mask	1
005-1	mapflag	mask	1
006-1	mapflag	mask	1

function	script	isDay	{
	.@daynight = getmapmask("001-1");
	if (.@daynight <= 4){
		return 1;
	}
	return 0;
}

function	script	setMapsCycle	{
	.@add = getarg(0);
	.@rem = getarg(1);
	
	setarray .@MAPS$, "001-1", "002-1", "003-1", "004-1", "005-1", "006-1";
	for (.@i = 0; .@i < getarraysize(.@MAPS$); ++.@i) {
		addremovemapmask .@MAPS$[.@i], .@add, .@rem;
	}
}

//non dialog npc
001-1,0,0,0	script	#[DAYNIGHT]	NPC127,{
	close;


OnClock0554:
	//lighter
	setMapsCycle 8, 16;
	end;

OnClock0556:
	//lighter
	setMapsCycle 4, 8;
	end;

OnClock0558:
	//lighter
	setMapsCycle 2, 4;
	end;

OnClock0600:
	//lightest minAlpha
	setMapsCycle 0, 2;
	end;

OnClock1754:
	//darker
	setMapsCycle 2, 0;
	end;

OnClock1756:
	//darker
	setMapsCycle 4, 2;
	end;

OnClock1758:
	//darker
	setMapsCycle 8, 4;
	end;

OnClock1800:
	//darkest MaxAlpha
	setMapsCycle 16, 8;
	end;

OnInit:
	initnpctimer;
	if(gettime(GETTIME_HOUR) > 18 || gettime(GETTIME_HOUR) < 6)
	{
		setMapsCycle 16, 8;
	}
	end;
}
