
function	script	Ferry	{
	set @Cost_tulimshar, 250;
	set @Cost_hurnscald, 250;
	set @Cost_nivalis, 250;
	set @Cost_kasinow, 300;
	set @Cost_gm, 0;
	mes l("Where do you want to travel?");
	next;
	if ($MAP_gm == 1) goto L_gm_menu;
	switch(select(
		l("Tulimshar (" + @Cost_tulimshar + "GP)"),
		l("Hurnscald (" + @Cost_hurnscald + "GP)"),
		l("Nivalis (" + @Cost_nivalis + "GP)"),
		l("Kasinow (" + @Cost_kasinow + "GP)"),
		l("GM-Island ( " + @Cost_gm + " GP )"),
		l("I thik i'll stay here")))
	{
		case 1:
			if (@loc == DOCK_tulimshar) goto L_AlreadyThere;
			if (Zeny < @Cost_tulimshar) goto L_NotEnoughGP;
			if ($MAP_tuliumshar == 0) goto L_no_warp;
			set Zeny, Zeny - @Cost_tulimshar;
			warp "022-1", 76, 72;
		case 2:
			if (@loc == DOCK_hurnscald) goto L_AlreadyThere;
			if (Zeny < @Cost_hurnscald) goto L_NotEnoughGP;
			if ($MAP_hurnscald == 0) goto L_no_warp;
			set Zeny, Zeny - @Cost_hurnscald;
			warp "008-1", 137, 64;
		case 3:
			if (@loc == DOCK_nivalis) goto L_AlreadyThere;
			if (Zeny < @Cost_nivalis) goto L_NotEnoughGP;
			if ($MAP_nivalis == 0) goto L_no_warp;
			set Zeny, Zeny - @Cost_nivalis;
			warp "019-1", 111, 95;
		case 4:
			if (@loc == DOCK_kasinow) goto L_AlreadyThere;
			if (Zeny < @Cost_kasinow) goto L_NotEnoughGP;
			if ($MAP_kasinow == 0) goto L_no_warp; 
			set Zeny, Zeny - @Cost_kasinow;
			warp "kasinow", 114, 49;
		case 5:
			if (Zeny < @Cost_gm) goto L_NotEnoughGP;
			if ($MAP_gm == 0) goto L_no_warp;
			set Zeny, Zeny - @Cost_gm;
			warp "gm", 0, 0;
		default:
			close;
	}
	close;

L_AlreadyThere:
	mes "You are already here!";
	close;

L_NotEnoughGP:
	mes l("You dont have enough money!");
	close;

L_no_warp:
	mes l("At this moment there isn't a ship travel to this place!");
	close;
}
