
function	script	Inn	{
	msqb l(@npcname$);
	mesq l("Would you like to rest? It's only " + @Cost + " gp.");
	next;
	switch (select (l("Yes"), l("No")))
	{
		case 1:
			if (Zeny < @Cost)
			{
				msqb l(@npcname$);
				mesq l("You don't have enough money to stay here.");
				close2;
				return;
			}
			set Zeny, Zeny - @Cost;
			heal 10000, 10000;

			msqb l(@npcname$);
			mesq l("Sleep well!");
			close2;
			return;
		default:
			msqb l(@npcname$);
			mesq l("See You.");
			close2;
			return;
	}
	return;
}
