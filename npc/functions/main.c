
function	script	getspellinvocation	{
    return "";
}

function	script	getactivatedpoolskilllist	{
    return "";
}

function	script	getunactivatedpoolskilllist	{
    return "";
}

function	script	poolskill	{
    return "";
}

function	script	unpoolskill	{
    return "";
}

function	script	isin	{
    if (getmapxy(.@mapName$, .@xpos, .@ypos, 0) != 0)
        return false;
    if (.@mapName$ != getarg(0))
        return false;
    if (.@xpos >= getarg(1) && .@xpos <= getarg(3) && .@ypos >= getarg(2) && .@ypos <= getarg(4))
        return true;
    return false;
}

function	script	isat	{
    if (getmapxy(.@mapName$, .@xpos, .@ypos, 0) != 0)
        return false;
    if (.@mapName != getarg(0))
        return false;
    if (.@xpos == getarg(1) && .@ypos == getarg(2))
        return true;
    return false;
}

function	script	sc_check	{
    return getscrate(getarg(0), 0);
}

function	script	npcwarp	{
    movenpc getarg(2), getarg(0), getarg(1);
}

function	script	mapexit	{
    mes "here must be quit?";
}

function	script	cmdothernpc	{
    donpcevent getarg(0) + "::OnCommand" + getarg(1);
}

function	script	getx	{
    if (getmapxy(.@mapName$, .@xpos, .@ypos, 0) != 0)
        return 0;
    return .@xpos;
}

function	script	gety	{
    if (getmapxy(.@mapName$, .@xpos, .@ypos, 0) != 0)
        return 0;
    return .@ypos;
}

// === GTMW STUFF ===

function	script	addbasicskills	{
    skill NV_BASIC, 9, 0;
    return;
}

function	script	addq	{
    return ("\"" + getarg(0) + "\"");
}

function	script	mesq	{
    mes "\"" + getarg(0) +  "\"";
    return;
}

function	script	mesqr	{
    mes getarg(0) +  "\"";
    return;
}

function	script	mesql	{
    mes "\"" + getarg(0);
    return;
}

function	script	str	{
    return "" + getarg(0);
}

function	script	msqb	{
	mes "[" + getarg(0) + "]";
	return;
}

function	script	addqb	{
    return ("[" + getarg(0) + "]");
}

function	script	addremovemapmask	{
    setmapmask getarg(0), (getmapmask(getarg(0)) | (getarg(1) + getarg(2))) ^ getarg(2);
    return;
}

function	script	menuint	{
    deletearray .@vals;
    .@menustr$ = "";
    .@cnt = 0;

    for (.@f = 0; .@f < getargcount(); .@f = .@f + 2)
    {
        if (getarg(.@f) != "")
        {
            .@menustr$ = .@menustr$ + getarg(.@f) + ":";
            .@vals[.@cnt] = getarg(.@f + 1);
            .@cnt ++;
        }
    }

    @menu = 255;
    @menuret = -1;
    select(.@menustr$);
    if (@menu == 255) return -1;

    @menu --;
    if (@menu < 0 || @menu >= getarraysize(.@vals)) return -1;

    @menuret = .@vals[@menu];
    return @menuret;
}

function	script	menustr	{
    deletearray .@vals$;
    .@menustr$ = "";
    .@cnt = 0;

    for (.@f = 0; .@f < getargcount(); .@f = .@f + 2)
    {
        if (getarg(.@f) != "")
        {
            .@menustr$ = .@menustr$ + getarg(.@f) + ":";
            .@vals$[.@cnt] = getarg(.@f + 1);
            .@cnt ++;
        }
    }

    @menu = 255;
    @menuret = -1;
    select(.@menustr$);
    if (@menu == 255)
        return "";

    @menu --;
    if (@menu < 0 || @menu >= getarraysize(.@vals$))
        return "";

    @menuret$ = .@vals$[@menu];
    return @menuret$;
}

