
function	script	Banker	{
	set @npcname$ , getarg(0);
	if (BankAccount == 0)
		goto L_Start;
	callsub S_MoveAccount;
	goto L_Start;

L_Start:
	msqb l(@npcname$);
	mes l("Welcome to the bank!");
	mes l("How can I help you?");
	menuint	l("Open my storage"), 0,
			l("Deposit"), 1,
			l("Withdraw"), 2,
			l("Check my balance"), 3,
			l("Change Bank Options"), 4,
			l("Nevermind"), 5;

	switch(@menuret)
	{
		case 0:
			openstorage;
			if (#BankOptions & OPT_STORAGE_CLOSE) 
			{
				close2;
				return;
			}
			goto L_Start;

		case 1:
			msqb l(@npcname$);
			@dep_with = 0;
			mes l("How much would you like to deposit?");
			goto L_menu;

		case 2:
			msqb l(@npcname$);
			@dep_with = 1;
			mes l("How much would you like to withdraw?");
			goto L_menu;
			
		case 3:
			goto L_Balance;

		case 4:
			goto L_Change;
	}
	return;

L_menu:
	next;
	menuint
		l("Other"), 0,
		l("5,000 GP"), 5,
		l("10,000 GP"), 10,
		l("25,000 GP"), 25,
		l("50,000 GP"), 50,
		l("100,000 GP"), 100,
		l("250,000 GP"), 250,
		l("500,000 GP"), 500,
		l("1,000,000 GP"), 1000,
		l("All of my money"), 1,
		l("I've changed my mind"), 2,
		l("Quit"), 3;
	if( (@menuret > 3 ) && (Zeny < @menuret) || (@menuret == 0 ) && (Zeny < 1))
		goto L_NoMoney;
	switch(@menuret)
	{
		case 0:
		L_input:
			input @Amount;
			if (@Amount >= 0)
				goto L_Continue;
			msqb l(@npcname$);
			mes l("I need a positive amount. What would you like to do?");
			menuint l("Go back"), 0,
					l("Try again"), 1,
					l("All of my money"), 2,
					l("Nevermind"), 3;
			switch(@menuret)
			{
				case 0:
					goto L_menu;
				case 1:
					goto L_input;
				case 2:
					goto L_all;
				case 3:
					return;
			}
		case 1:
		L_all:
			set @Amount, Zeny;
			goto L_Continue;
		case 2:
			goto L_Start;
		case 3:
			return;
		case 5000:
			set @Amount, 5000;
			goto L_Continue;
		case 10000:
			set @Amount, 10000;
			goto L_Continue;
		case 25000:
			set @Amount, 25000;
			goto L_Continue;
		case 50000:
			set @Amount, 50000;
			goto L_Continue;
		case 100000:
			set @Amount, 100000;
			goto L_Continue;
		case 250000:
			set @Amount, 250000;
			goto L_Continue;
		case 500000:
			set @Amount, 500000;
			goto L_Continue;
		case 1000000:
			set @Amount, 1000000;
			goto L_Continue;
	}

L_Continue:
	//deposit == 0 | withraw == 1
	if (@dep_with == 0)
	{
		if (Zeny < @Amount)
			goto L_NoMoney;
		set Zeny, Zeny - @Amount;
		set #BankAccount, #BankAccount + @Amount;
		goto L_Balance;
	}else{
		if (#BankAccount < @Amount)
			goto L_NoMoney;
		set Zeny, Zeny + @Amount;
		set #BankAccount, #BankAccount - @Amount;
		goto L_Balance;
	}


L_Balance:
	msqb l(@npcname$);
	mes l("Your current bank balance is:");
	mes l(#BankAccount + " GP");
	if (#BankOptions & OPT_BANK_CLOSE) return;
	goto L_Start;

L_Nev:
	msqb l(@npcname$);
	mes l("Goodbye then.");
	return;

L_NoMoney:
	msqb l(@npcname$);
	mes l("Oh dear, it seems that you don't have enough money.");
	goto L_Start;

S_MoveAccount:
	set #BankAccount, #BankAccount + BankAccount;
	set BankAccount, 0;
	return;

L_Change:
	setarray @menuitems$, l("Keep the current settings"), l("Close NPC dialog after selecting storage option"), l("Close NPC dialog after checking your balance");
	if (#BankOptions & OPT_STORAGE_CLOSE) set @menuitems$[1], l("Return to main menu after leaving storage");
	if (#BankOptions & OPT_BANK_CLOSE) set @menuitems$[2], l("Return to main menu after leaving bank");
	menu
		@menuitems$[0], L_Start,
		@menuitems$[1], L_Change_Storage,
		@menuitems$[2], L_Change_Bank;

L_Change_Storage:
	set #BankOptions, (#BankOptions ^ OPT_STORAGE_CLOSE);
	goto L_Start;

L_Change_Bank:
	set #BankOptions, (#BankOptions ^ OPT_BANK_CLOSE);
	goto L_Start;
}
