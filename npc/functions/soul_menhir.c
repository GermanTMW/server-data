//l(en)

function	script	SoulMenhir	{
    mes l("[Soul Menhir]");
    mes l("(A mystical aura surrounds this stone. You feel mysteriously attracted to it. Something tells you to touch it. What do you do?)");
    next;
	switch (select (l("Touch it."), l("Leave it alone.")))
	{
		case 1:
			switch (Menhir_Activated)
			{
				case 0:
					mes l("[Soul Menhir]");
					mes l("(You touch the mysterious stone. Somehow it feels warm and cold at the same time.)");
					next;

					mes l("[Soul Menhir]");
					mes l("(Suddenly a strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone.)");
					next;

					mes l("[Soul Menhir]");
					mes l("(As suddenly as the feeling started it stops. The strange attraction is away from one moment to the next and the menhir feels like just an ordinary stone.)");
					next;

					set Menhir_Activated, 1;
					break;
				case 1:
					mes l("[Soul Menhir]");
					mes l("(A strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone. As suddenly as the feeling started it stops.)");
					next;
					break;
			}
			switch(@x + @y)
			{
				case 0:
					set @n, rand(getarraysize(@Xs));
					set @x, @Xs[@n];
					set @y, @Ys[@n];
					break;
			}
			savepoint @map$, @x, @y;
			return;
		case 2:
			return;
	}
	//should never get reached, but save is save :)
	return;
}
