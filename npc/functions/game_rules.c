
function	script	GameRules	{

	mes l("Players breaking the following rules may be banned for any length of time (even permanently) or have their characters reset at a GM's discretion:");
    mes l("1) Do not abuse other players. Insults, swearing, and the like are not to be directed towards a particular person or group.");
    mes l("2) No bots – including ##Bany##b AFK activity or automated actions of any sort.");
    mes l("3) No spamming or flooding (including messages, whispers, and trade requests).");
    mes l("4) No begging.");
    mes l("5) Speak ##Bonly##b English or ##b German in the public chat.");
    mes l("6) Treat others how you would like to be treated.");
    mes l("AFK botting will be determined by talking to players who are moving and/or attacking.");
    mes l("Automated following will be determined by observation.");
	set @read, 1;
	set TUT_var, TUT_var | 1;
	return;
}
