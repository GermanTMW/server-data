0103-1,114,47,0	script	[.]#1	NPC127,{
	mes l("Ferry") + aRight();
	close;
}

0103-1,102,51,0	script	[.]#2	NPC127,{
	mes aLeft() + l("Kasinow");
	mes l("Animal park");
	mes aDown();
	close;
}

0103-1,98,47,0	script	[.]#3	NPC127,{
	mes aUp();
	mes l("Alchemist house");
	close;
}

009-1,46,34,0	script	[.]#4	NPC127,{
	aUp();
	mes l("Mine");
	mes l("Ferry") + aRight();
	mes aLeft() + l("Forest");
	mes l("Tulimshar");
	mes aDown();
	close;
}

009-1,92,47,0	script	[.]#5	NPC127,{
	mes l("Blacksmith");
	close;
}

009-1,78,34,0	script	[.]#6	NPC127,{
	mes l("Hospital");
	close;
}

009-1,61,34,0	script	[.]#7	NPC127,{
	mes l("Archer Shop");
	close;
}

009-1,71,34,0	script	[.]#8	NPC127,{
	mes l("Wyara's Alchemist house");
	close;
}

009-1,36,34,0	script	[.]#9	NPC127,{
	mes l("Inn");
	close;
}
