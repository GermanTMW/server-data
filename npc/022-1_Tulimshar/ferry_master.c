
022-1,73,71,0	script	[Ferry Master]	NPC138,{
	msqb l("Ferry Master");
	mesq l("Hello! Do you need something?");
	next;
	switch(select(
		l("How do I use the ferry?"),
		l("Nothing I guess")))
	{
		case 1:
			msqb l("Ferry Master");
			mesq l("You'll setup on the dock and select your destination. Each destination has an associated price that you need to pay. You don't have to go anywhere. Choosing the current port will result in nothing.");
		default:
			close;
	}
	close;


OnInit:
	.sex = G_MALE;
	end;
}
