011-4,71,34,0 script Xmas9 105,{
        if(XMASQUEST9 == 1) goto L_only_once;
        mes "[Helper Santa]";
        mes "It's Christmas.";
        mes "The time for gifts and family reunions";
        next;

        mes "[Helper Santa]";
        mes "You get a gift from me.";
	mes "The kind will depend on whether";
	mes "you've been nice or naughty.";
        next;

        menu "Want one?",L_nicenaughty,"No thanks.",L_no;

L_nicenaughty:
	set XMASQUEST9,1;
	mes "[Helper Santa]";
	mes "In my book you're marked as...";
        next;
	set @temp,rand(9);
	if(@temp == 6) goto L_naughty;
	goto L_nice;

L_nice:
	mes "[Helper Santa]";
	mes "...nice!";
        @temp = rand(17);
	next;
        if(@temp == 0) goto L_nice_0;
        if(@temp == 1) goto L_nice_1;
	if(@temp == 2) goto L_nice_2;
	if(@temp == 3) goto L_nice_3;
	if(@temp == 4) goto L_nice_4;
	if(@temp == 5) goto L_nice_5;
	if(@temp == 6) goto L_nice_6;
	if(@temp == 7) goto L_nice_7;
	if(@temp == 8) goto L_nice_8;
	if(@temp == 9) goto L_nice_9;
        if(@temp == 10) goto L_nice_10;
        if(@temp == 11) goto L_nice_11;
        if(@temp == 12) goto L_nice_12;
        if(@temp == 13) goto L_nice_13;
        if(@temp == 14) goto L_nice_14;
        if(@temp == 15) goto L_nice_15;
        if(@temp == 16) goto L_nice_16;
        if(@temp == 17) goto L_nice_17;

L_naughty:
	mes "[Helper Santa]";
	mes "...naughty!";
	set @temp,rand(6);
	next;
	if(@temp == 0) goto L_naughty_0;
	if(@temp == 1) goto L_naughty_1;
	if(@temp == 2) goto L_naughty_2;
	if(@temp == 3) goto L_naughty_3;
	if(@temp == 4) goto L_naughty_4;
	if(@temp == 5) goto L_naughty_5;
	if(@temp == 6) goto L_naughty_6;

L_naughty_0:
	mes "[Helper Santa]";
	mes "Ho ho ho!";
	mes "I borrowed some of your money for";
	mes "my present budget this year.";
	set Zeny,Zeny-rand(500,1500);
	close;

L_naughty_1:
	mes "[Helper Santa]";
	mes "You will get a spanking";
	next;
	mes "[Helper Santa]";
	mes "Remember to be nice to people!";
	atcommand strcharinfo(0)+":@die ";
	close;

L_naughty_2:
	mes "[Helper Santa]";
	mes "But I won't mind!";
	mes "Have a char reset as a present."
	next;
	mes "[Helper Santa]";
	mes "Abracadabra!";
	atcommand strcharinfo(0)+":@charstreset "+strcharinfo(0);
	atcommand strcharinfo(0)+":@charskreset "+strcharinfo(0);
	close;

L_naughty_3:
	mes "[Helper Santa]";
	mes "You will now get randomly warped!";
	next;
	atcommand strcharinfo(0)+":@jump ";
	mes "[Helper Santa]";
	mes "Merry Christmas!";
	close;

L_naughty_4:
	mes "[Helper Santa]";
	mes "Your Sex will now change!";
	mes "You always get naughty things for naughty things.";
	mes "It's karma!";
	next;
	mes "[Helper Santa]";
	mes "Ho ho ho!";
	atcommand strcharinfo(0)+":@charchangecharsex() "+strcharinfo(0);
	close;

L_naughty_5:
	mes "[Helper Santa]";
	mes "I'll now kill all monsters on this map.";
	next;
	mes "[Helper Santa]";
	mes "Done!";
	atcommand strcharinfo(0)+":@killmonster2 ";
	close;

L_naughty_6:
	mes "[Helper Santa]";
	mes "I'll diss you around the server.";
	next;
	mes "[Helper Santa]";
	mes "Ho ho ho!";
	atcommand strcharinfo(0)+":@kami "+strcharinfo(0)+" is not in the holiday mood.";
	close;

L_nice_0:
	mes "[Helper Santa]";
	mes "So you wanted money?.";
	set Zeny,Zeny+rand(1,1000000);
	close;

L_nice_1:
	mes "[Helper Santa]";
	mes "These hats are really hot now!";
	getitem 1205,1;
	close;

L_nice_2:
	mes "[Helper Santa]";
	mes "Axe Hats are always on the cutting edge!";
	getitem 616,1;
	close;

L_nice_3:
        mes "[Helper Santa]";
	mes "You just won a Pirate Hat!";
	getitem 617,1;
	close;

L_nice_4:
        mes "[Helper Santa]";
	mes "Now you can Goggle all you want.";
	getitem 618,1;
	close;

L_nice_5:
        mes "[Helper Santa]";
	mes "Let's Goggle all night long!";
	getitem 619,1;
	close;

L_nice_6:
        mes "[Helper Santa]";
	mes "You just won a Circlet!";
	getitem 620,1;
	close;

L_nice_7:
        mes "[Helper Santa]";
	mes "This patch is not an eyesore.";
	getitem 621,1;
	close;

L_nice_8:
        mes "[Helper Santa]";
	mes "Bandana time!";
	getitem 622,1;
	close;

L_nice_9:
        mes "[Helper Santa]";
	mes "5000 exp!";
	getexp 5000,0;
	close;

L_nice_10:
        mes "[Helper Santa]";
	mes "Soft packages are always nice.";
	getitem 1202,1;
	close;

L_nice_11:
        mes "[Helper Santa]";
	mes "Chuck Norris style!";
	getitem 1203,1;
	close;

L_nice_12:
        mes "[Helper Santa]";
	mes "Even Rudolph would be envious now.";
	getitem 1204,1;
        close;

L_nice_13:
        mes "[Helper Santa]";
	mes "Now you can become a Santa too!";
	getitem 1206,1;
	close;

L_nice_14:
        mes "[Helper Santa]";
	mes "Now you can became a Santa too!";
	getitem 511,1;
	close;

L_nice_15:
        mes "[Helper Santa]";
	mes "These gloves should keep you warm.";
	getitem 564,1;
	close;

L_nice_16:
        mes "[Helper Santa]";
	mes "This should keep you warm.";
	getitem 624,1;
	close;

L_nice_17:
        mes "[Helper Santa]";
	mes "This should keep you warm.";
	getitem 564,1;
	close;

L_only_once:
        mes "[Helper Santa]";
        mes "You already got one from me.";
        close;

L_no:
	mes "[Helper Santa]";
	mes "You're not in the Christmas spirit?";
	mes "I hope you get into this gift extravaganza soon.";
	close;
}
