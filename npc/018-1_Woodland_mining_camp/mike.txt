018-1,77,60,0	script	Mike	NPC113,{

	mes "[Mike]";
	mes "\"Ich brauche Schwarze SkorpionenStachel, um Medizin für meine Schwester daraus zu machen.\"";
	next;

        @dq_level = 40;
        @dq_cost = 16;
        @dq_count = 4;
        @dq_name$ = "BlackScorpionStinger";
        @dq_friendly_name$ = "Schwarze SkorpionenStachel";
        @dq_money = 2500;
        @dq_exp = 500;

        callfunc "DailyQuest";

        next;

	mes "\"Ich hoffe ich habe bald genug...\"";
	close;

}
