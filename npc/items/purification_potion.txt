
function	script	usePurificationPotion	{
	if (isat("011-1", 88,67))
		goto L_Wyara_Pond;

L_NoUse:
        message strcharinfo(0), "Du wei�t nicht was du damit machen sollst.";
	getitem "PurificationPotion", 1;
        return;

L_Wyara_Pond:
        @Q_MASK = NIBBLE_2_MASK;
        @Q_SHIFT = NIBBLE_2_SHIFT;
        @Q_status = (QUEST_MAGIC2 & @Q_MASK) >> @Q_SHIFT;

	if (@Q_status < 1)
		goto L_NoUse;
        if (@Q_status <= 2)
		set @Q_status, @Q_status + 1;

        message strcharinfo(0), "Du laesst den Trank langsam in den Teich.";

	set QUEST_MAGIC2,
		(QUEST_MAGIC2 & ~(@Q_MASK)
		| (@Q_status << @Q_SHIFT));
	return;
}
