
function	script	RestrictedItem	{
	if (!@minLvl) set @minLvl, 60;
	if (debug || getgmlevel() >= @minLvl) end; // If the active character is staff, do nothing.
	message strcharinfo(0), l("This item repells you with extreme force. It does not seem to be meant for you.");
	unequipbyid @slotId;
	if (getgmlevel()) end;
	atcommand strcharinfo(0) + " : @wgm Restricted item '" + @itemId + "' used by character '" + strcharinfo(0) + "'.";
	atcommand strcharinfo(0) + " : @l Restricted item '" + @itemId + "' used by character '" + strcharinfo(0) + "'.";
	end;
}
