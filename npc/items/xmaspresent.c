function	script	WeihnachtsGeschenk	{
	getinventorylist;
	if ((@inventorylist_count == 100) || ((@inventorylist_count == 99) && (countitem(4515) > 1)))
	{
		message strcharinfo(0), l("You need more space in your Inventory to open the Present!");
		getitem 4515, 1;
		return;
	}
	set @temp,rand(4);
	switch(@temp)
	{
		case 0:
			message strcharinfo(0), l("You open the present and find a ") + getitemname(721);
			getitem 721, 1;
			getitem 2206, 1;
			return;

		case 1:
			message strcharinfo(0), l("You open the present and find a ") + getitemname(2210);
		   	getitem 2210, 1;
		   	getitem 2206, 1;
			return;

		case 2:
			message strcharinfo(0), l("You open the present and find a ") + getitemname(1204);
			getitem 1204, 1;
			getitem 2206, 1;	
			return;

		case 3:
			message strcharinfo(0), l("You open the present and find a ") + getitemname(628);
			getitem 628, 1;
			getitem 2206, 1;
			return;
	}
}
