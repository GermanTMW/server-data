#!/bin/bash

typeset -i cFiles txtFiles SHOW
cFiles=0;
txtFiles=0;
SHOW=0;


check(){
	for SubFile in $TDIR/*; do
		if [ -d "${SubFile}" ]; then
			TDIR=$SubFile;
			check;
		fi;
		if [[ ${SubFile: -2} == ".c" ]]; then
			cFiles=$cFiles+1;
			if [[ $SHOW == 1 || $SHOW == 3 ]]; then
				echo "C   $SubFile";
			fi
		elif [[ ${SubFile: -4} == ".txt" ]]; then
			txtFiles=$txtFiles+1;
			if [[ $SHOW == 2 || $SHOW == 3 ]]; then
				echo "TXT $SubFile";
			fi
		fi
	done;
}


case $1 in
	c)
		SHOW=1;
	;;
	txt)
		SHOW=2;
	;;
	all)
		SHOW=3;

	;;
	*)
		SHOW=0;
	;;
esac

for File in *; do
	TDIR=$File;
	check;
done;
echo ".c   files: $cFiles"
echo ".txt files: $txtFiles"

