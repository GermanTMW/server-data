
027-1,83,84,0	script	[Dawn]	NPC146,{

if (QUEST_bonearrows == 1) goto L_quietsch;
if (QUEST_bonearrows == 2) goto L_bonearrow;

	mes "~~QUIIIIEETSCH~~";
	next;
	mes "\"Oh man...";
	mes "Mit dem Stumpfen Messer hier komm ich nicht weiter...\"";
	next;
	menu 
		"\"Ich bring dir ein schärferes...\"", L_setone,
		"...",-;
	close;

L_quietsch:
	mes "\"Hast du ein [Scharfes Messer] gefunden?\"";
	menu
		"Yop, hier hast du.",L_ymesser,
		"ehm... Nein...",-;
	mes "\"Bring mir doch bitte ein [Scharfes Messer]\"";
	close;

L_ymesser:
	if (countitem(522) < 1) goto L_keinmesser;
	set QUEST_bonearrows, 2;
	mes "\"Ah, Das Messer Schneidet schon viel besser.\"";
	delitem 522, 1;
	next;
	goto L_bonearrow;
	
L_setone:
	mes "\"Sehr nett von dir!\"";
	set QUEST_bonearrows, 1;
	close;

L_bonearrow:
	mes "\"wenn du mir 20 [Knochen] Bringst kann ich dir 40 [KnochenPfeile] daraus machen...\"";
	next;
	mes "\"Möchtest du ein paar [KnochenPfeile]?\"";
	menu
		"Sicher doch, hier sind die 20 [Knochen].",L_bonearrow_yep,
		"Ich glaub ich hab noch nicht genug [Knochen]",-;
	close;

L_bonearrow_yep:
	getinventorylist;
	if (countitem(775) < 20) goto L_NotEnoughitems;
	if ((@inventorylist_count == 100) && (countitem(775) > 20)) goto L_tomanyitems;
	mes "\"Oh, Die [knochen] sehen gut aus, hier hast du deine Pfeile :)\"";
	next;
	delitem 775, 20;
	getitem 2239, 40; 
	next;
	mes "\"Komm wieder wenn du Neue Pfeile Brauchst.\"";
	close;

L_keinmesser:
	mes "Du hast kein [Scharfes Messer] in deinem Inventar...";
	close;

L_NotEnoughitems:
	mes "\"Du hast nicht genug [Knochen], ich brauche 20 davon...\"";
	close;

L_tomanyitems:
	mes "\"tut mir leid, du hast zu viel in deinem Inventar...\"";
	close;
}
