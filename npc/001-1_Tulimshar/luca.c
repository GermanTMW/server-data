
001-1,108,55,0	script	Luca	NPC102,{



    msqb(l("Luca the Hunter"));
    mesq(l("Ouch! It hurts, this wound I got from battle."));
    next;
    menu
    l("Tell me about it"), L_Exp,
    l("Nevermind"), L_Nev;

L_Exp:
	msqb(l("Luca the Hunter"));
	mesq(l("I was just coming back from a long journey. I ran into a group of scorpions and started fighting them for experience."));
        next;
	msqb(l("Luca the Hunter"));
        mesq(l("Then, out of a cave came a BLACK one! I had NEVER seen it before!"));
	next;
	msqb(l("Luca the Hunter"));
	mesq(l("Luckily I had a camera with me! Here's a picture of it... Let me find it, I put it in my pocket somewhere..."));
	next;
	msqb(l("Luca the Hunter"));
	mesq(l("Oh man! My pockets have been ripped clean off!"));
	close;

L_Nev:
	msqb(l("Luca the Hunter"));
	mesq(l("Hmpf!"));
	close;

L_Teach_soon:
	msqb(l("Luca the Hunter"));
	mesq(l("You've grown quite a bit stronger since I first saw you, strolling around town like that! You know, if you train a little more, maybe we can help each other out a little?"));
        close;

L_Teach:
	if (getskilllv(SKILL_POOL))
		goto L_Teachmore;

	msqb(l("Luca the Hunter"));
	mesq(l("Hey there! You've become quite the adventurer, haven't you? I think it's time that someone taught you some basic skills."));
        next;
	msqb(l("Luca the Hunter"));
	mesq(l("If you want, I can help with that!"));
        next;
        menu
		l("Sure, that sounds fun!"), -,
		l("I'm not interested."), L_Nev;

	msqb(l("Luca the Hunter"));
	mesq(l("All right! It's not all that easy, though. First you have to learn how to focus. That's because you can learn many skills, but you can't focus on all of them all the time."));
        next;

	msqb(l("Luca the Hunter"));
	mesq(l("When you can do that, you can learn some real skills, and when you know them, you can come back to me to tell me which ones you want to focus on. Deal?"));
        next;

        menu
		l("Yeah!"), L_Teach0_follow,
		l("Certainly!"), L_Teach0_follow,
		l("Wait... what do you mean?"), -,
		l("Please explain some more."), -,
		l("No."), L_Nev;

	callsub S_explain;

L_Teach0_follow:
	msqb(l("Luca the Hunter"));
        mesq(l("Great! Now, before I can give you that skill, I have to make sure that those stories about you are all true and that you really are experienced enough."));
        next;
	msqb(l("Luca the Hunter"));
        mesq(l("So I'm going to ask you some easy questions."));
        next;

	msqb(l("Luca the Hunter"));
        mesq(l("First, who is the man who guards the entrance to the old Tulimshar underground arena?"));
        next;
        input @anser$;
        if (@answer$ != "Phaet" && @answer$ != "phaet")
		goto L_wronganser;

	msqb(l("Luca the Hunter"));
        mesq(l("Good. Next, what's the name of the pipe-smoker who lives on top of Lore Mountain and makes leathery goods there?"));
        next;
        input @anser$;
        if (@answer$ != "Pachua" && @answer$ != "pachua")
		goto L_wronganser;

	msqb(l("Luca the Hunter"));
        mesq(l("Great! One more: Who was the adventurer who built Dimond's Cove for Dimond?"));
        next;
        input @anser$;
        if (@answer$ != "Merlin" && @answer$ != "merlin")
		goto L_wronganser;


	msqb(l("Luca the Hunter"));
        mes(l("Luca laughs in excitement."));
        mesq(l("All right! You're the real thing, my friend!"));
        next;

	msqb(l("Luca the Hunter"));
        mesq(l("Now stand over there. This should only take a second..."));
        mes(l("He takes off a pendant he is wearing and holds it up."));
        next;

	msqb(l("Luca the Hunter"));
        mesq(l("See that fang at the end of the chain? Keep staring at it."));
        next;

	msqb(l("Luca the Hunter"));
        mes(l("He starts swinging the pendant back and forth."));
        mesq(l("And whatever you do, don't blink. Don't look away, and don't blink."));
        next;

	msqb(l("Luca the Hunter"));
        mes(l("The world slows down around you as you focus on the pendant going back..."));
        next;

        mes(l("and forth..."));
        next;

        mes(l("and back..."));
        next;

        mes(l("and forth..."));
        next;

        mes(l("and back..."));
        next;

        mes(l("and..."));
        next;

        mes(l("You feel relaxed."));
        next;

	msqb(l("Luca the Hunter"));
        mesq(l("... still there?"));
        mes(l("You hear the sound of fingers snapping. Why would someone do that on such a calm day?"));
        next;

	msqb(l("Luca the Hunter"));
        mesq(l("Oh, good... I think it worked. Nice job!"));
        mes(l("He grins."));
        msqb(l("You gain 10,000 experience points"));
        msqb(l("You learned Skill Focus"));
        addtoskill SKILL_POOL, 1;
        getexp 10000, 0;
        next;

        goto L_Teachmore2;

L_wronganser:
	msqb(l("Luca the Hunter"));
        mesq(l("No, that was wrong. I suppose you're not as experienced as I thought you'd be."));
        close;

S_explain:
        msqb(l("Luca the Hunter"));
        mesq(l("Alright, what do you want to know?"));
        next;
S_explain_loop:
	menu
        	l("What's skill focus?"), L_explain_focus,
        	l("What skills are there?"), L_explain_skills,
        	l("How do skills work?"), L_explain_work,
        	l("Thanks, I think I got it!"), -;
	return;

L_explain_focus:
        msqb(l("Luca the Hunter"));
        mesq(l("Well, you can learn many, many skills. But you can't really use all of them at once, not even old Auldsbels' head is big enough for that! So you have to focus."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("But you can change that focus whenever you want. It works like this: You come to me, you tell me what you want to focus on, and we do exercises until it's stuck in your head."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("If there's not enough space in your head, you'll first have to stop thinking about some other thing. There's an exercise I learned to do that, so when you talk to me you can just let me know what you want to forget."));
        next;
        msqb(l("Luca the Hunter"));
        if (getskilllv(SKILL_POOL) == 0)
	        mesq(l("Right now you can't focus on anything yet, so I'll first have to teach you a simple skill to keep your focus."));
        if (getskilllv(SKILL_POOL) == 1)
	        mesq(l("Right now you can only focus on one thing at a time. So if you want to change what your focus is, you have to forget whatever else you've focused on."));
        if (getskilllv(SKILL_POOL) > 1)
	        mesq(l("You can focus on " + getskilllv(SKILL_POOL) + " skills right now."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("You can come back to me to focus or unfocus a skill whenever you want, I'm always here."));
        next;
        goto S_explain_loop;

L_explain_skills:
        msqb(l("Luca the Hunter"));
        mesq(l("Well, I don't really know... lots, I think. I can teach you Brawling, which is good when you've run out of arrows or don't have a weapon around."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("But there are other skills around. Some mages need special skills to concentrate, so ask around in that crowd, if that's your thing."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("I've also heard that there is someone in Hurnscald who can help you make your body more resistant against some things. But body and mind belong together, so you probably have to focus for that, too."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("Other than that, just ask around! There have to be some people who can teach you something..."));
        next;
        goto S_explain_loop;

L_explain_work:
        msqb(l("Luca the Hunter"));
        mesq(l("Well, skills allow you to be better in some things, or allow to do what you couldn't do without them. When you've learned a skill and are focusing on it, then it affects what you do."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("Now, when you start out with a skill, you won't be very good at it yet. But if you keep practicing you'll learn how to get better."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("My old teacher always called that getting `skill points' and that you can see them with [F5], whoever that is."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("She also said that with this [F5] thing you can get better at those skills, and that it costs you as many skill points as the level you want to go to."));
        next;
        msqb(l("Luca the Hunter"));
        mesq(l("She always would have one cactus potion too many... but she was an amazing teacher."));
        mes(l("He nods emphatically."));
        next;
        goto S_explain_loop;

L_Teachmore:
        msqb(l("Luca the Hunter"));
        mesq(l("Good to see you again!"));
        next;

L_Teachmore2:
        menu
        	l("I would like to focus."), L_Focus,
        	l("I would like to unfocus."), L_Unfocus,
                l("Can you teach me a skill?"), L_Teach_brawling,
                l("Can you explain skills again?"), L_Teachmore_explain,
                l("I'm done for now, thanks!");
	close;

L_Teachmore_explain:
	callsub S_explain;
        goto L_Teachmore2;

L_Focus:

L_Unfocus:

L_Teach_brawling:
        close;

}
