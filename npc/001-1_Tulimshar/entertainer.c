//left for now, (skills need rework first)
001-1,30,45,0	script	[Entertainer]	NPC165,{
	set @EMOTE_SKILL, 1;
	if (getskilllv(@EMOTE_SKILL) > 0) goto L_Has;

	msqb(l("Entertainer"));
	mesq(l("Yes how can I help you?"));
	menu
		l("How are you showing emotions above your head?"), L_Learn,
		l("Never mind."), -;
	close;

L_Learn:
	msqb(l("Entertainer"));
	mesql(l("They are called emotes, I use them to express how I am feeling."));
	mesqr(l("I can teach you if you'd like?"));
	menu
		l("Yes please."), L_Learn2,
		l("I don't see the point right now."), -;
	close;

L_Learn2:
	addbasicskills;
	msqb(l("Entertainer"));
	mesql(l("All you have to do is press alt and a number."));
	mes(l("The number determines what emotion will be shown."));
	mesqr(l("Some clients will also show an emote shortcut-bar with the F12 button"));
	close;

L_Has:
	msqb(l("Entertainer"));
	mesq(l("The life of Entertainer isn't so easy..."));
	mes(l("*sighs*"));
	close;


OnTimer3000:
	emotion rand(0,11);
	setnpctimer 0;

OnInit:
	initnpctimer;

}

