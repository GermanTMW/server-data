
001-1,110,71,0	script	[Sandra]	NPC114,{
	if(Scorp == 2) goto L_Done;
	if(Scorp == 1) goto L_Progress;

	set @temp, rand(4);
	if(@temp == 0) goto L_Opening0;
	if(@temp == 1) goto L_Opening1;
	if(@temp == 2) goto L_Opening2;
	if(@temp == 3) goto L_Opening3;

L_Opening0:
	msqb(l("Sandra"));
	mesq(l("In the outskirts of Tulimshar, there are some scorpions... I need  help!  Will you help?"));
	next;
	goto L_Ask;

L_Opening1:
	msqb(l("Sandra"));
	mesq(l("When you venture to the outskirts of Tulimshar, you can spot scorpions. Will you help me kill some?"));
	next;
	goto L_Ask;

L_Opening2:
	msqb(l("Sandra"));
	mesq(l("The Scorpion Stinger carries many properties used in potions. Would you get some for me?"));
	next;
	goto L_Ask;

L_Opening3:
	msqb(l("Sandra"));
	mesq(l("You look sturdy enough, will you help me get something?"));
	next;
	goto L_Ask;

L_Ask:
	menu
		l("Yes."), L_Yes,
		l("No."), L_No;

L_Yes:
	set @temp, rand(3);
	if(@temp == 0) goto L_Req0;
	if(@temp == 1) goto L_Req1;
	if(@temp == 2) goto L_Req2;

L_Req0:
	msqb(l("Sandra"));
	mesq(l("I need 5 Scorpion Stingers and 1 Red Scorpion Stinger."));
	next;
	goto L_Set;

L_Req1:
	msqb(l("Sandra"));
	mesq(l("I heard a while ago that stingers from scorpions can be used for medical purposes. I need you to help me get 5 Scorpion Stingers and 1 Red Scorpion Stinger."));
	next;
	goto L_Set;

L_Req2:
	msqb(l("Sandra"));
	mesq(l("Bring me 5 Scorpion Stingers and 1 Red Scorpion Stinger. I'll give you something if you do!"));
	next;
	goto L_Set;

L_Set:
	set Scorp,1;
	msqb(l("Sandra"));
	mesq(l("Please get them for me!"));
	close;

L_Progress:
	if (countitem("ScorpionStinger") < 5) goto L_NotEnough;
	if (countitem("RedScorpionStinger") < 1) goto L_NotEnough;
	msqb(l("Sandra"));
	mesql(l("Exellent!"));
	mesqr(l("You brought me 5 Scorpion Stingers and 1 Red Scorpion Stinger!"));
	getinventorylist;
	if ((@inventorylist_count - (countitem("ScorpionStinger") == 5) - (countitem("RedScorpionStinger") == 1)) > 99 - (countitem("Arrow") == 0)) goto L_TooMany;
	delitem "ScorpionStinger", 5;
	delitem "RedScorpionStinger", 1;
	getitem "Bow", 1;
	getitem "Arrow", 100;
	set Scorp, 2;
	close;

L_NotEnough:
	msqb(l("Sandra"));
	mesq(l("Please hurry and bring me 5 Scorpion Stingers and 1 Red Scorpion Stinger."));
	close;

L_Done:
	msqb(l("Sandra"));
	mesq(l("Thank you for all your help!"));
	close;

L_No:
	close;

L_TooMany:
	next;
	msqb(l("Sandra"));
	mesq(l("You don't have room for my reward. I'll wait until you do."));
	close;
}

