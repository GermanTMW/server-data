
001-1,48,52,0	script	[Aidan]	NPC102,{
	if(MPQUEST == 0) goto Register;
	msqb(l("Aidan the Monster Guide"));
	mes(l("Du hast momentan " +Mobpt+ " Monsterpunkte. Diese Punkte verdienst du dir, wenn du Monster tötest."));
	close;
Register:
	msqb(l("Aidan the Monster Guide"));
	mes(l("Oh my, you don't seem to be registered as a Quest Participant. Would you like to register?"));
	next;
Choice:
	menu 
		l("Register"),L_R,
		l("Not at the moment"),L_N,
		l("Information"),L_I;
L_R:
	msqb(l("Aidan the Monster Guide"));
	mes(l("Give me a second to look over your paperwork."));
	next;
	msqb(l("Aidan the Monster Guide"));
	mes(l("Well, looks like you qualify!"));
	mes(l("Welcome to the questing world!"));
	set MPQUEST,1;
	close;
L_N:
	msqb(l("Aidan the Monster Guide"));
	mes(l("Very well, you don't know what you're missing."));
	close;
L_I:
	msqb(l("Aidan the Monster Guide"));
	mes(l("Here in The Mana World, there are certain rewards for your vanquishing of foes."));
	mes(l("For example, there are Monster Points; every monster you kill has a certain amount of points that get added to your account."));
	mes(l("The more points you have, the more expensive things you can buy using them."));
	next;
	msqb(l("Aidan the Monster Guide"));
	mes(l("So whaddaya say, sign up won't you?"));
	next;
	goto Choice;
}

