
001-1,57,33,0	script	[Trader]	NPC115,{
	set @TRADE_SKILL, 2;
	if (getskilllv(@TRADE_SKILL) > 0) goto L_Has;

	msqb(l("Trader"));
	mesq(l("Hello. I came here to trade wares with the people of Tulimshar. Unfortunately for you, I've traded everything I had."));
	next;
	menu
		l("Oh. I'll go then."), -,
		l("You don't have anything?"), L_More;
	close;

L_More:
	msqb(l("Trader"));
	mesq(l("No. I have nothing for you. Except..."));
	next;
	menu
		l("Yes?"), L_Except,
		l("Ok then."),  -;
	close;

L_Except:
	msqb(l("Trader"));
	mesq(l("I could teach you how to trade. It'll cost you 5€."));
	next;
	menu
		l("Sure."), L_Teach,
		l("No thank you."), -;
	close;

L_Teach:
	if (Zeny < 5) goto L_NotEnoughMoney;
	set Zeny, Zeny - 5;
	addtoskill @TRADE_SKILL, 1;
	msqb(l("Trader"));
	mesql(l("You can initiate trade with someone by right-clicking on them and choosing Trade."));
	mes(l("You'll both add the items and set the money you're putting up then press propose trade."));
	mesqr(l("After both parties have proposed their side, you can both review the trade, and then accept or reject by closing the window."));
	next;
	msqb(l("Trader"));
	mesql(l("Items added to the trade cannot be removed, and so mistakes have to be dealt with by canceling the trade."));
	mesqr(l("You need to press the change button to let the other person know about GP changes."));
	close;

L_Has:
	msqb(l("Trader"));
	mesq(l("I still have nothing to trade..."));
	close;

L_NotEnoughMoney:
	msqb(l("Trader"));
	mesq(l("You don't have enough money."));
	close;
}
