
001-1,40,73,0	script	[Bard]	NPC152,{
	setarray @songs$, addq(l("There once was a bard, who had it hard, because a man in dark green, was very mean.")), addq(l("At Hurnscald inn, there was a person of fairest skin, declining wedding bands from quite a many hands.")), addq(l("As the Sun sets down in the forest's brown, she whom the fragrance holds counts her gold."));
	@name$ = l("Bill Ballshaker the Bard");
	msqb @name$;
	if (Sex)
		mesq(l("Greetings, traveler!  Have you come to listen to my stories?"));
	if (!Sex)
		mesq(l("Greetings, fair lady!  Well met, on such a lovely day!  Might I entertain you with a tale or two?"));
	next;

L_Main:
	switch(select(l("Sing me a song, lute man!"),
		 l("Have you heard any news?"),
		 l("Farewell!")))
	{
		case 1:
			msqb(l(@name$));
			set @id, rand(3);
			mes @songs$[@id];
			goto L_Main;
		case 2:
			msqb(l(@name$));
			mesq(l("News I have for you indeed, lest you have already overheard (for then it would no longer be news to you!)"));
			next;
			msqb(l(@name$));
			mesq(l("Rumor has it that an ancient source of magic, a Mana Seed, has been sighted in the west, beyond the fair town of Hurnscald."));
			next;
			MAGIC_FLAGS = MAGIC_FLAGS | MFLAG_MANASEED_RUMOUR;
			msqb(l(@name$));
			mesq(l("Whence it came, nobody knows... but they say that sometimes such Mana Seeds may choose a powerful individual to impart some of its mystic power to!"));
			next;
			msqb(l(@name$));
			mesq(l("I wonder who might be qualified?"));
			next;
			if ($NEWSNPC_MSG$ == "") goto L_Main;
			msqb(l(@name$));
			mesql(l("There was something else..."));
			next;
			mesqr(l("..." + $NEWSNPC_MSG$ + ""));
			next;
			goto L_Main;
		default:
			close;
	}
	close;
}
