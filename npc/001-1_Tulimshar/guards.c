
001-1,48,79,0	script	[Ekinu]	NPC104,{
	msqb(l("Ekinu the Town Guard"));
	mesq(l("What a disaster... I can't believe that we managed to rebuild the town so quickly."));
	next;
	switch(select(l("Can you give me any tips?"), l("Do you have any information?"), l("What disaster?")))
	{
		case 1:
			switch(rand(7))
			{
				case 0:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("Let me think of something... Oh! Do NOT attack the red or black scorpions unless you're sure you can kill it!"));
				case 1:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("Try to carry spare food when on quests or in dungeons – they will come in handy. I always take beer, but don't tell the Sergeant..."));	
				case 2:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("If you ever get lost, check your map to get your bearings and figure out where you need to go."));
				case 3:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("When in a dungeon, monsters are more aggressive – and often more powerful – than if they were outside."));
				case 4:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("When gambling in casinos, make sure to not bet too much or you might become broke."));
				case 5:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("Never underestimate your enemy."));
				default:
					msqb(l("Ekinu the Town Guard"));
					mesq(l("When fighting more than one enemy, try to focus on one at a time."));
			}
			close;
		case 2:
			msqb(l("Ekinu the Town Guard"));
			mesq(l("Everyone seems to want information... You won't get it – at least not here. I'm not even sure what you mean, to be honest."));
	
		default:
			msqb(l("Ekinu the Town Guard"));
			mesq(l("What do you mean, 'what disaster?'  Weren't you there?  That huge volcano ex...rusion... – whatever they call it – and then the earthquake!"));
			next;
			msqb(l("Ekinu the Town Guard"));
			mesq(l("Half the town was reduced to rubble!  Somehow, no one was seriously injured, so I guess we were lucky..."));
			next;
			msqb(l("Ekinu the Town Guard"));
			mesq(l("Well, the mayor rebuilt everything quickly, but I still have no idea where he got all the GP from..."));
	}
	close;
}

001-1,40,79,0	script	[Ryan]	NPC104,{
	msqb(l("Ryan the Town Guard"));
	mes(l("Zzzz... Zzzz..."));
	next;
	msqb(l("Ryan the Town Guard"));
	mesql(l("Hmmmmmmh...?"));
	mesqr(l("Oh, what? I wasn't sleeping! I was just resting my eyes!"));
	close;
}
