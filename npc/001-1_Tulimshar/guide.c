
001-1,32,27,0	script	[Ian]	NPC102,4,4,{
	if (TUT_var & 1 == 1 && TUT_var & 2 == 0 && BaseLevel >= 10) callsub S_Grad;
	msqb(l("Ian the Guide"));
	mesq(l("Would you like to know about something?"));
	next;

L_Menu_A:
    menu
        l("Fighting"), L_Fight,
        l("Items"), L_Items,
        l("Monsters"), L_Monster,
        l("Stylist"), L_Style,
        l("Quests"), L_Quests,
        l("NPCs"), L_NPC,
        l("Commands"), L_Comm,
        l("Attributes"), L_Stats,
        l("Magic"), L_Magic,
        l("Quick Keys"), L_Key,
        l("Laws"), L_Laws,
        l("I know everything!"), L_Know;
	close;

L_Fight:
    msqb(l("Ian the Guide"));
    mesql(l("People live in this world by living off of monsters."));
    mesqr(l("You can fight monsters and even players by hitting the [CTRL] key, or left mouse click."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("No need to press [CTRL] all the time."));
    mesqr(l("Your character will attack automatically."));
    next;
    goto L_Menu_A;

L_Items:
    msqb(l("Ian the Guide"));
    mesql(l("There are three types of items."));
    mesqr(l("They can be Consumables, Equipment or Miscellaneous"));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Consumable items such as Potions, can be used only once."));
    mesqr(l("After use, they will disappear from your inventory."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Equipment items like Armors, Weapons, Accessories"));
    mesqr(l("can be equipped for fashionable purposes or to raise your status."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Miscellaneous items such as maggot slime, are used"));
    mesqr(l("in creating other items, or just to trade and sell."));
    next;
    goto L_Menu_A;

L_Monster:
    msqb(l("Ian the Guide"));
    mesql(l("In every world, there are beasts. Monsters can be found almost anywhere!"));
    mesqr(l("To fight them, please read [Fighting] if you do not know how."));
    next;
    mesq(l("There a several types of monsters, Aggressive, Neutral, and Assistants."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Aggressive monsters know that they are always in danger"));
    mes(l("so therefore they always keep their guard up,"));
    mesqr(l("making them attack anybody in sight."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Neutral monsters tend to just lounge around until attacked."));
    mesqr(l("They will leave everything alone unless they are threatened."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Assistants are monsters who help each other."));
    mesqr(l("You should always check how many are around you before attacking a single one!"));
    next;
    goto L_Menu_A;

L_Style:
    msqb(l("Ian the Guide"));
    mesql(l("The stylist NPCs will cut and perm your hair!"));
    mesqr(l("They are known for their hair growth formula."));
    next;
    goto L_Menu_A;

L_Quests:
    msqb(l("Ian the Guide"));
    mesql(l("There are people in the world in need of help!"));
    mes(l("Most of these people aren't afraid to give rewards to those who help them."));
    mesqr(l("So be nice and help people along the way!"));
    next;
    goto L_Menu_A;

L_NPC:
    msqb(l("Ian the Guide"));
    mesql(l("NPCs [Non Playable Characters] are people who are always in the game,"));
    mesqr(l("tending to many varieties of services from just chatting to helping others."));
    next;
    goto L_Menu_A;

L_Comm:
    msqb(l("Ian the Guide"));
    mesq(l("/clear clears the text box."));
    mesq(l("/help displays the client commands (ones starting with a /) in the chat box."));
    mesq(l("/whisper [name] allows you to message someone privately."));
    mesq(l("/who displays the current number of online users."));
    mesq(l("/where displays the current map's name."));
    next;
    goto L_Menu_A;

L_Stats:
    msqb(l("Ian the Guide"));
    mesq(l("People vary greatly by how much strength, agility, dexterity, intelligence, vitality, and luck they have."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Strength helps you carry items, and it also allows you to hit harder – but it is not too helpful if you focus on missile weapons."));
    mes(l("More agility allows you to attack faster, and to dodge attacks more easily."));
    mesqr(l("Your dexterity determines how likely you are to hit a monster, and how effective you are with missile weapons."));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("Vitality determines how resistant to injuries you are, and how much damage you can take before you die."));
    mes(l("Intelligence is useful for alchemy and magic, but there are few opportunities for either at the moment."));
    mesqr(l("Luck determines many small things, including the likelihood of both recieving and dealing critical hits"));
    next;
    msqb(l("Ian the Guide"));
    mesql(l("I recommend that you practice your dexterity, since some of the more dangerous monsters are very hard to hit otherwise."));
    mesqr(l("Don't bother trying to work on your luck, and your intelligence is probably not something anyone cares about either."));
    next;
    goto L_Menu_A;

L_Magic:
    msqb(l("Ian the Guide"));
    mesql(l("In decades past, there was a Mana Seed south of Tulimshar, from which people could draw magical energies. Unfortunately, the seed is gone."));
    mesqr(l("However, I've overheard the bard mentioning something about this recently... if you are interested in magic, try talking to him!"));
    next;
    goto L_Menu_A;

L_Key:
    msqb(l("Ian the Guide"));
    mesq(l("There are many key combinations, press F1 for a short list of them!"));
    next;
    goto L_Menu_A;

L_Laws:
    msqb(l("Ian the Guide"));
    mesq(l("The constable can help you with them."));
    next;
    goto L_Menu_A;

L_Know:
	close;

S_Grad:
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	msqb(l("Ian the Guide"));
	mesq(l("Hey, you've been doing good, let me give you this."));

	getitem "GraduationCap", 1;
	set TUT_var, TUT_var | 2;
	next;

	return;

L_TooMany:
	msqb(l("Ian the Guide"));
	mesq(l("I wanted to give you something, but you don't have room for it."));
	next;
	return;
    
OnTouch:
    if (TUT_var & 1 == 1) close;
	GameRules;
	mes(l("Ian the Guide, can help you to know more about the game."));
	close;
}

