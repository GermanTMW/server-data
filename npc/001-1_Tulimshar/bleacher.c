//REWRITE (cards removing or npc removing)
001-1,27,53,0	script	[Candide]	NPC156,{
	msqb(l("Candide"));
	mesql(l("I've figured out how that volcanic ash bleaches cloth."));
	mesqr(l("I could do it for you, but I'll need three piles of ash and 5,000 GP for supplies per item."));
	next;

	if (countitem(701) < 3) goto L_No_Ash;

	if (Zeny < 5000) goto L_No_Money;

	menu l("I'd like to bleach something."), -,
		 l("No thanks."), L_Close;

L_bleach_menu:
	msqb(l("Candide"));
	mesq(l("What would you like to bleach?"));
	next;

	menu
		l("Cotton shirt."), L_cottonshirt,
		l("V-Neck sweater."), L_vneck,
		l("Turtleneck sweater."), L_tneck,
		l("Cotton shorts."), L_shorts,
		l("Cotton skirt."), L_skirt,
		l("Miniskirt."), L_miniskirt,
		l("Tank top."), L_tanktop,
		l("Short tank top."), L_tanktop_short,
		l("Silk robe."), L_robe,
		l("Cotton headband."), L_cotton_headband,
		l("Desert hat."), L_desert_hat,
		l("Cotton boots."), L_cotton_boots,
		l("Cotton gloves."), L_cotton_gloves,
		l("Fine dress."), L_kleid,
		l("Never mind."), -;
	goto L_Close;

L_cottonshirt:
	set @normal, 1202;
	set @dyeBase, 2050;
	goto L_choose_color;

L_vneck:
	set @normal, 624;
	set @dyeBase, 2060;
	goto L_choose_color;

L_tneck:
	set @normal, 564;
	set @dyeBase, 2070;
	goto L_choose_color;

L_shorts:
	set @normal, 586;
	set @dyeBase, 2110;
	goto L_choose_color;

L_skirt:
	set @normal, 632;
	set @dyeBase, 2100;
	goto L_choose_color;

L_miniskirt:
	set @normal, 771;
	set @dyeBase, 2170;
	goto L_choose_color;

L_tanktop:
	set @normal, 688;
	set @dyeBase, 2090;
	goto L_choose_color;

L_tanktop_short:
	set @normal, 689;
	set @dyeBase, 2120;
	goto L_choose_color;

L_robe:
	set @normal, 720;
	set @dyeBase, 2080;
	goto L_choose_color;

L_cotton_headband:
	set @normal, 724;
	set @dyeBase, 2140;
	goto L_choose_color;

L_desert_hat:
	set @normal, 723;
	set @dyeBase, 2130;
	goto L_choose_color;

L_cotton_boots:
	set @normal, 735;
	set @dyeBase, 2150;
	goto L_choose_color;

L_cotton_gloves:
	set @normal, 741;
	set @dyeBase, 2160;
	goto L_choose_color;

L_kleid:
	set @normal, 783;
	set @dyeBase, 2240;
	goto L_choose_color;	
	
L_choose_color:
	msqb(l("Candide"));
	mesq(l("And the color?"));
	next;

	menu l("Red."), -,
		 l("Green."), -,
		 l("Dark Blue."), -,
		 l("Yellow."), -,
		 l("Light Blue."), -,
		 l("Pink."), -,
		 l("Black."), -,
		 l("Orange."), -,
		 l("Purple."), -,
		 l("Dark Green."), -;
	 
	set @del, @dyeBase + @menu - 1;

	if (countitem(@del) == 0) goto L_No_Item;

	if (countitem(701) < 3) goto L_No_Ash;

	if (Zeny < 5000) goto L_No_Money;

	delitem @del, 1;
	getitem @normal, 1;
	delitem 701, 3;
	set Zeny, Zeny - 5000;

L_Again:
	msqb(l("Candide"));
	mesq(l("Would you like to bleach something else?"));
	next;

	menu l("Yes."), L_bleach_menu,
		 l("No, thank you."), L_Close;

L_No_Item:
	msqb(l("Candide"));
	mesq(l("You don't have one of those."));
	next;

	goto L_Again;

L_No_Ash:
	msqb(l("Candide"));
	mesql(l("You don't have enough ash for me to bleach anything."));
	mesqr(l("I need three piles."));
	next;

	goto L_Close;

L_No_Money:
	msqb(l("Candide"));
	mesql(l("You don't have enough gold for me to bleach anything."));
	mesqr(l("I need 5,000 GP for supplies."));
	next;

L_Close:
	msqb(l("Candide"));
	mesq(l("Come again."));
	close;
}
