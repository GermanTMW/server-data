
001-1,95,29,0	script	[Aisha]	NPC108,{
	switch(rand(8))
	{
		case 1:
			msqb(l("Aisha"));
			mesq(l("Want to play ball with me?"));
		case 2:
			msqb(l("Aisha"));
			mesq(l("There are so many monsters; I hate scorpions!"));
		case 3:
			msqb(l("Aisha"));
			mesq(l("When I grow up, I want to be strong enough to kill a scorpion!"));
		case 4:
			msqb(l("Aisha"));
			mesq(l("Mommy told me that you can drop the things that monsters sell. Oops, the other way round."));
		case 5:
			msqb(l("Aisha"));
			mesq(l("Have you tried to eat a roasted maggot?  They are sooo yummy!  And you feel much healthier afterwards, too!"));
		case 6:
			msqb(l("Aisha"));
			mesq(l("I want to be a Doctor when I grow up."));
		case 7:
			msqb(l("Aisha"));
			mesq(l("Maggots are soo slimey!"));
		default:
			msqb(l("Aisha"));
			mesq(l("That volcano was sooo scary!  The earth was shaking and everything was breaking down... but now they have rebuilt everything."));
	}
	close;

OnInit:
	.sex = G_MALE;
	end;
}

001-1,39,67,0	script	[Nina]	NPC103,{
	msqb(l("Nina"));
	switch(rand(10))
	{
		case 1:
			mesq(l("If I've learned anything in school, it is that Grenxen founded Tulimshar."));
		case 2:
			mesq(l("When I was collecting stones I saw a red scorpion"));
		case 3:
			mesq(l("When you're talking to someone it is polite to say his name first."));
		case 4:
			mesq(l("I have a Skorpion doll!"));
		case 5:
			mesq(l("I always carry with me a few cactus drinks. Unfortunately they are quite heavy..."));
		case 6:
			mesq(l("If you feel bad, eat something. It helps you to restore your health faster."));
		case 7:
			mesq(l("Grenxen is the demon who founded Tulimshar."));
		case 8:
			mesq(l("The eruption of the volcano was terrible; almost all Tulimshar was destroyed. But the mayor had everything set up quite quickly."));
		default:
			mesq(l("I know a very bad word. But I must not say it, because monsters will come and get me if I do!"));
 	  	 	next;
			switch(select(l("A bad word?"),l("Oh, you better keep it for yourself then.")))
			{
				case 1:
					msqb(l("Nina"));
					mesq(l("Yes, I heard my mother say it once. And she made me promise her to never say it. Never."));
					next;
					switch(select(l("If I promise to never tell anyone, can you tell me the word?"),
						l("I understand, you do not need to tell me..."),l("Goodbye!")))
					{ 
						case 1:
							msqb(l("Nina"));
							mesq(l("No."));
						case 2: 
							//may need to change spells arnt work anymore...
							msqb(l("Nina"));
							mes(l("Nina looks around as she hushes you to silence. After a few seconds, she whispers to you."));
							mesq(l("I like you!  So I will tell you the bad word. But you can't tell anyone else about it!  The bad word is '" + getspellinvocation("aggravate") + "'."));
							next;

							msqb(l("Nina"));
							mes(l("Terrified, she looks around once more."));
							mesq(l("But you mustn't tell anyone!"));
						default:
							close;
					}
				default:
					close;
			}
	}
	close;

OnInit:
	.sex = G_MALE;
	end;
}
