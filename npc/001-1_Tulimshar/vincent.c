
001-1,135,42,0	script	[Vincent]	NPC113,{
	if(BugLeg == 2) goto L_Done;
	if(BugLeg == 1) goto L_Progress;

	set @temp, rand(4);
	if(@temp == 0) goto L_Opening1;
	if(@temp == 1) goto L_Opening2;
	if(@temp == 2) goto L_Opening3;
	if(@temp == 3) goto L_Opening4;

L_Opening1:
	msqb(l("Vincent"));
	mesq(l("I just need 10 more Bug Legs to finish my action figure!"));
	next;
	goto L_Ask;

L_Opening2:
	msqb(l("Vincent"));
	mesq(l("This maggot action figure is awesome! I just need to attach 10 Bug Legs."));
	next;
	goto L_Ask;

L_Opening3:
	msqb(l("Vincent"));
	mesq(l("This is a great action figure! A must have! All I need is a few parts..."));
	next;
	goto L_Ask;

L_Opening4:
	msqb(l("Vincent"));
	mesq(l("Can you get me 10 Bug Legs? I need them to replace the action figure parts."));
	next;
	goto L_Ask;

L_Ask:
	msqb(l("Vincent"));
	mesq(l("Will you help me find 10 Bug Legs?"));
	next;
	menu
		l("Yes."), L_Sure,
		l("No."), -;
	close;

L_Sure:
	set BugLeg, 1;
	set @temp,rand(4);
	if(@temp == 0) goto L_Req1;
	if(@temp == 1) goto L_Req2;
	if(@temp == 2) goto L_Req3;
	if(@temp == 3) goto L_Req4;

L_Req1:
	msqb(l("Vincent"));
	mesq(l("Thank you!"));
	next;
	goto L_Wait;

L_Req2:
	msqb(l("Vincent"));
	mesq(l("I don't know how to thank you enough!"));
	next;
	goto L_Wait;

L_Req3:
	msqb(l("Vincent"));
	mesq(l("I will thank you when I get them!"));
	next;
	goto L_Wait;

L_Req4:
	msqb(l("Vincent"));
	mesq(l("I'm sure I will give a small reward."));
	next;
	goto L_Wait;

L_Wait:
	msqb(l("Vincent"));
	mesq(l("Now please go get me 10 Bug Legs."));
	close;

L_Progress:
	if(countitem("BugLeg") >= 10) goto L_Have;
	msqb(l("Vincent"));
	mesq(l("Please help me collect 10 Bug Legs! I need them to complete my action figure."));
	close;

L_Have:
	msqb(l("Vincent"));
	mesq(l("Excellent! Finally I can complete the model! :D"));
	next;

	if(countitem("BugLeg") < 10) goto L_Progress;
	delitem "BugLeg", 10;
	set Zeny, Zeny + 1000;
	set BugLeg, 2;

	msqb(l("Vincent"));
	mesq(l("Here you go, a little of my appreciation!"));
	mes(l("")); 
	msqb(l("1000€"));
	close;

L_Done:
	msqb(l("Vincent"));
	mesq(l("Thanks for your help!"));
	close;
}

