009-1,81,41,0	script	[Milly]	NPC114,{
	mes "[Milly]"; 
 	mes "\"Hallo.\""; 
 	next; 
 	if (Inspector == 1) goto L_NohMask; 
 	close; 
  
L_NohMask: 
	menu 
 		"Hast du in letzter Zeit etwas auffälliges gesehen?", L_NohMask_Strange, 
 		"Weißt du etwas über die letzten Diebstähle?", L_NohMask_Robbery, 
 		"Hallo.", -; 
 	close; 
  
L_NohMask_Strange: 
 	mes "[Milly]"; 
 	mes "\"Ich habe nichts seltsames gesehen.\""; 
 	close; 
  
L_NohMask_Robbery: 
 	mes "[Milly]"; 
 	mes "\"Nein, tut mir leid.\""; 
 	close; 

}
