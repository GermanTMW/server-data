009-1,88,27,0	script	[Sabine]	NPC106,{
 	mes "[Sabine]"; 
	mes "\"Ist dieser Platz nicht schön? Ich liebe es hier rumzuhängen!\""; 
 	next; 
 	if (Inspector == 1) goto L_NohMask; 
 	close; 
  
L_NohMask: 
 	menu 
 		"Hast du in letzter Zeit etwas Seltsames gesehen?", L_NohMask_Strange, 
 		"Weist du etwas über die letzen Diebstähle?", L_NohMask_Robbery, 
 		"Ja, ist er.", -; 
 	close; 
  
L_NohMask_Strange: 
 	mes "[Sabine]"; 
 	mes "\"Ich habe nicht's Seltsames gesehen.\""; 
 	close; 
  
L_NohMask_Robbery: 
 	mes "[Sabine]"; 
 	mes "\"Nein, tut mir leid.\""; 
 	close; 
} 
