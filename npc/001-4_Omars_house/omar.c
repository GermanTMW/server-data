function	script	KadiyaSubquestConsts	{
        @Q_kadiya_status = (QUEST_MAGIC2 & NIBBLE_3_MASK) >> NIBBLE_3_SHIFT;

        @Q_STATUS_NONE = 0;
        @Q_STATUS_KNOWS_MOPOX = 1;
        @Q_STATUS_MADE_MOPOX = 2;
        @Q_STATUS_DIDNT_DRINK = 3;
        @Q_STATUS_WANTS_CHOCOCAKE = 4;
        @Q_STATUS_WANTS_ORANGECUPCAKE = 5;
        @Q_STATUS_COMPLETED = 6;
        @Q_STATUS_COMPLETED_ELANORE = 7;
        @Q_STATUS_COMPLETED_NOELANORE = 8;
        @Q_STATUS_COMPLETED_POST_ELANORE = 9;

        if (((QUEST_MAGIC2 & NIBBLE_1_MASK) >> NIBBLE_1_SHIFT) < 4)
        	set @Q_kadiya_status, @Q_STATUS_NONE;

        if (((QUEST_MAGIC2 & NIBBLE_1_MASK) >> NIBBLE_1_SHIFT) > 4)
        	set @Q_kadiya_status, @Q_STATUS_COMPLETED_POST_ELANORE;

	return;
}


001-4,25,21,0	script	Omar	NPC162,{
        callfunc "ElanoreFix";
	callfunc "KadiyaSubquestConsts";

        @Q_status = @Q_kadiya_status;

        if (@Q_status >= @Q_STATUS_COMPLETED)
		goto L_cured;

        if (@Q_status == @Q_STATUS_DIDNT_DRINK)
		goto L_didnt_drink;

        if (@Q_status > @Q_STATUS_DIDNT_DRINK)
		goto L_make_food;

	msqb(l("Omar"));
	mesq(l("Welcome to my humble abode."));
        next;

        menu
		l("Well met!  May I ask who you are?"), L_omar,
		l("What's wrong with your daughter?"), L_kadiya_sick,
                l("Thank you!"), -;
	close;

L_omar:
	msqb(l("Omar"));
        mesq(l("My name is Omar;  I am a trader of oils and spices."));
        close;

L_kadiya_sick:
	msqb(l("Omar"));
        mesq(l("Kadiya has caught Ponderpox, I fear."));
        mes(l("He sighs."));
        mesq(l("She's has been feverish for many days now. I wish there were something I could do."));
        next;

        menu
		l("Have you asked Elanore the healer?"), L_sick_elanore,
		l("How about the Hurnscald hospital?"), L_sick_hospital,
		l("Can I help?"), L_sick_self,
		l("I'm sorry to hear that."), -;

	msqb(l("Omar"));
        mesq(l("Well, she is a strong girl. I am sure that she will get over it eventually."));
        mes(l("He smiles, but you see doubt in his eyes."));
        close;

L_sick_elanore:
	msqb(l("Omar"));
        mesq(l("Elanore?"));
        mes(l("He frowns."));
        mesq(l("She knows nothing. I see no point in talking to her."));
        close;

L_sick_hospital:
	msqb(l("Omar"));
        mesq(l("Hurnscald is too far away. I don't think that it would be good for her to go on such a long trip."));
        close;

L_sick_self:
	msqb(l("Omar"));
        mesq(l("You are very kind, but I don't think there is anything you can do."));
        close;

L_didnt_drink:
        msqb(l("Omar"));
        mesq(l("So she has Mopox, not Ponderpox, you say?  Hmm. That is much harder to cure, I think."));
        mesq(l("I am grateful for the effort you put into brewing a potion for her. But it does smell vile..."));
        next;

        msqb(l("Omar"));
        mesq(l("I promised my dear wife that I would never make Kadiya eat or drink anything she doesn't like. So I won't force her to drink this."));
        next;

        msqb(l("Omar"));
        @Q_status = (@Q_STATUS_WANTS_CHOCOCAKE + rand(2));
        callsub S_Update_Var;

        if (@Q_status == @Q_STATUS_WANTS_ORANGECUPCAKE)
        	mesq(l("If only we could make it smell and taste like orange cupcakes... She really loves those cupcakes."));
        if (@Q_status == @Q_STATUS_WANTS_CHOCOCAKE)
        	mesq(l("If only this were a chocolate cake and not a potion... She really loves chocolate cakes."));
        close;

L_make_food:
        msqb(l("Omar"));
        mesq(l("I should stay here to watch over her. I wish I could think of a way to convince her to drink the potion..."));
        if (@Q_status == @Q_STATUS_WANTS_ORANGECUPCAKE)
        	mesq(l("Alas, it is not an orange cupcake."));
        if (@Q_status == @Q_STATUS_WANTS_CHOCOCAKE)
        	mesq(l("If only it were to smell and taste like her favourite chocolate cake..."));
        close;

L_cured:
        if (@Q_status == @Q_STATUS_COMPLETED)
		goto L_cured_choice;
        msqb(l("Omar"));
        mesq(l("She is sleeping now, but she seems to be much better. I am sure that she will be up and running around again soon."));
        close;

L_cured_choice:
        msqb(l("Omar"));
        mesq(l("I am very grateful for your help. I really was sure that it was Ponderpox, not Mopox, that she had."));
        next;

        menu
		l("Oh, it was nothing."), L_cured_nothing,
                l("I didn't do it alone; Elanore helped."), L_cured_elanore,
                l("That will be 5000 GP."), -;

        msqb(l("Omar"));
        mesq(l("Ah, certainly."));
        mes(l("He hands you a small bag of money."));
        next;

        @Q_status = @Q_STATUS_COMPLETED_NOELANORE;
        callsub S_Update_Var;

        Zeny = Zeny + 5000;
        close;

L_cured_nothing:
        msqb(l("Omar"));
        mesq(l("No, this wasn't nothing. She is everything to me. Here, you deserve a reward."));
        mes(l("He hands you a bag of money containing 10,000 GP and two pearls."));
        mesq(l("And feel free to drop by again whenever you would like!"));

        @Q_status = @Q_STATUS_COMPLETED_NOELANORE;
        callsub S_Update_Var;

        getitem "Pearl", 2;
        Zeny = Zeny + 10000;
        close;

L_cured_elanore:
        msqb(l("Omar"));
        mes(l("Omar frowns."));
        mesq(l("That witch?  You worked with her without telling me?"));
        mes(l("He looks over to his daughter, concern suddenly in his eyes."));
        mesq(l("She is looking better, though..."));
        next;

        msqb(l("Omar"));
        mes(l("Omar shakes his head."));
        mesq(l("I will have to think about this. Please leave me alone."));
        next;

        @Q_status = @Q_STATUS_COMPLETED_ELANORE;
        callsub S_Update_Var;

        close;

S_Update_Var:
	set QUEST_MAGIC2,
		(QUEST_MAGIC2 & ~(NIBBLE_3_MASK)
		| (@Q_status << NIBBLE_3_SHIFT));
	return;
}


001-4,31,20,0	script	_______Kadiya	NPC174,{
        callfunc "ElanoreFix";
        @Q_MASK = NIBBLE_3_MASK;
        @Q_SHIFT = NIBBLE_3_SHIFT;

	callfunc "KadiyaSubquestConsts";
        @Q_status = @Q_kadiya_status;

        if (@Q_status >= @Q_STATUS_COMPLETED)
		goto L_cured;

        mes(l("You see a young girl lying in bed. At first she doesn't seem to notice you approaching."));
        mes(l("Finally, she turns her head towards you. Judging from the sweat on her forehead and look in her eyes, she is suffering from some kind of fever."));
        next;

	msqb(l("Kadiya"));
	mesq(l("Hello..."));
	mes(l("She says it in a tiny voice."));
        next;

        @M_NAME = 1;
        @M_CANDY = 2;
        @M_POTION = 3;
        @M_CHOCOCAKE = 4;
        @M_CUPCAKE = 5;

        setarray @choice$, addq(l("Hello!  What's your name?")), addq(l("Would you like some candy?")), "", "", "", "", "", "";
        setarray @choice_idx, @M_NAME, @M_CANDY, 0, 0, 0, 0, 0, 0;
        @choices_nr = 2;

        if (countitem("MopoxCurePotion") == 0)
        	goto L_M_no_cure;

        @choice_idx[@choices_nr] = @M_POTION;
        @choice$[@choices_nr] = addq(l("This potion will cure your illness!"));
        @choices_nr = @choices_nr + 1;

L_M_no_cure:

        if ((countitem("LacedChocolateCake") == 0) || (@Q_status != @Q_STATUS_WANTS_CHOCOCAKE))
        	goto L_M_no_chococake;

        @choice_idx[@choices_nr] = @M_CHOCOCAKE;
        @choice$[@choices_nr] = addq(l("Would you like special chocolate cake?"));
        @choices_nr = @choices_nr + 1;

L_M_no_chococake:

        if ((countitem("LacedOrangeCupcake") == 0) || (@Q_status != @Q_STATUS_WANTS_ORANGECUPCAKE))
        	goto L_M_no_cupcake;

        @choice_idx[@choices_nr] = @M_CUPCAKE;
        @choice$[@choices_nr] = addq(l("Would you like special orange cupcake?"));
        @choices_nr = @choices_nr + 1;

L_M_no_cupcake:

        @choice_idx[@choices_nr] = 0;
        @choice$[@choices_nr] = addq(l("Goodbye."));
        @choices_nr = @choices_nr + 1;

        menu
		@choice$[0], -,
		@choice$[1], -,
		@choice$[2], -,
		@choice$[3], -,
		@choice$[4], -,
		@choice$[5], -;

        @choice = @choice_idx[@menu - 1];

        if (@choice == @M_NAME)		goto L_name;
        if (@choice == @M_CANDY)	goto L_no_candy;
        if (@choice == @M_POTION)	goto L_Potion;
        if (@choice == @M_CHOCOCAKE)	goto L_chococake;
        if (@choice == @M_CUPCAKE)	goto L_cupcake;

        close;

L_name:
	msqb(l("Kadiya"));
        mes(l("She smiles a faint smile."));
        mesq(l("My name is Kadiya."));

        close;

L_no_candy:
	msqb(l("Kadiya"));
        mes(l("She hesitates for a moment."));
        mesq(l("No, thank you. I don't think I should."));

	close;

L_Potion:
	msqb(l("Kadiya"));
        if (@Q_status < @Q_STATUS_DIDNT_DRINK)
                @Q_status = @Q_STATUS_DIDNT_DRINK;
        callsub S_Update_Var;                

        mes(l("Kadiya grimaces and pushes the bottle away."));
        mesq(l("That smells terrible!"));
        next;

	msqb(l("Kadiya"));
        mes(l("She pulls the sheets over her head."));
        mesq(l("I won't drink that!"));
        next;

        close;

L_chococake:
        delitem "LacedChocolateCake", 1;
        goto L_do_cure;

L_cupcake:
        delitem "LacedOrangeCupcake", 1;
        goto L_do_cure;

L_do_cure:
        if (@Q_status < @Q_STATUS_COMPLETED)
                @Q_status = @Q_STATUS_COMPLETED;
        callsub S_Update_Var;                
	msqb(l("Kadiya"));
        mes(l("Kadiya's eyes widen."));
        mesq(l("Oh!  Daddy, is it okay if I eat this?"));
        mes(l("Omar nods and smiles."));
        mesq(l("You should eat a bit to regain your strength, sweetheart."));
        next;

	msqb(l("Kadiya"));
        mes(l("Kadiya devours your gift in just a handful of bites."));
        next;

	msqb(l("Kadiya"));
        mes(l("She smiles."));
        mesq(l("Thank you! I feel better already!"));
        next;

        msqb(l("Kadiya"));
        mes(l("She looks much healthier, too. Omar walks over and touches her forehead."));
        mesq(l("Now that is funny – your fever has gone down quite a bit!"));
        mes(l("He smiles at you."));
        mesq(l("Thank you for your help!"));
        next;

        msqb(l("Kadiya"));
        mesq(l("You should get some sleep now, sweetheart. I'm sure that you will be better soon."));
        close;

L_Goodbye:
	msqb(l("Kadiya"));
        mes(l("Goodbye!"));

        close;

L_cured:
        msqb(l("Kadiya"));
        mes(l("Kadiya seems to be sleeping calmly. She looks much less sweaty than earlier; unless you are very much mistaken, her fever has disappeared."));
        close;

S_Update_Var:
	set QUEST_MAGIC2,
		(QUEST_MAGIC2 & ~(@Q_MASK)
		| (@Q_status << @Q_SHIFT));
	return;
}
