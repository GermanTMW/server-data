
008-1,0,0,0,0	monster	Flower	1014,10,20,0,Mob008-1::On1014
008-1,0,0,0,0	monster	Pinkie	1018,18,20,0,Mob008-1::On1018
008-1,0,0,0,0	monster	SpikyMushroom	1019,18,20,0,Mob008-1::On1019
008-1,0,0,0,0	monster	Fluffy	1020,5,30,0,Mob008-1::On1020
008-1,0,0,0,0	monster	Mauve	1029,2,270000,180000,Mob008-1::On1029
008-1,0,0,0,0	monster	SilkWorm	1035,2,60000,30000,Mob008-1::On1035
008-1,0,0,0,0	monster	Clover	1037,2,0,1000,Mob008-1::On1037
008-1,0,0,0,0	monster	Squirrel	1038,10,30,20,Mob008-1::On1038
008-1,0,0,0,0	monster	Biene	1062,10,30,20,Mob008-1::On1062
008-1,0,0,0,0	monster	Schmetterling	1063,20,30,20,Mob008-1::On1063
008-1,0,0,0,0	monster	Hase	1064,30,30,30,Mob008-1::On1064

008-1,0,0,0	script	Mob008-1	NPCMINUS1,{
On1064:
	set @mobId, 1064;
	MobPoints;
	end;
	
On1014:
	set @mobId, 1014;
	MobPoints;
	end;

On1018:
	set @mobId, 1018;
	MobPoints;
	end;

On1019:
	set @mobId, 1019;
	MobPoints;
	end;

On1020:
	set @mobId, 1020;
	MobPoints;
	end;

On1029:
	set @mobId, 1029;
	MobPoints;
	end;

On1035:
	set @mobId, 1035;
	MobPoints;
	end;

On1037:
	set @mobId, 1037;
	MobPoints;
	end;

On1038:
	set @mobId, 1038;
	MobPoints;
	end;
	
On1062:
	set @mobId, 1062;
	MobPoints;
	end;

On1063:
	set @mobId, 1063;
	MobPoints;
	end;
}
