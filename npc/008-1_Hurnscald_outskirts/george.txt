
008-1,136,36,0	script	[George 2]	NPC138,{
	if (getequipid(equip_head) == 617) goto L_Pirate;
	if (getequipid(equip_head) == 622) goto L_Bandana;
	if (getequipid(equip_head) == 621) goto L_EyePatch;
	mes "[George der Pirat]";
	mes "\"Arrrrh! 'Das ist eine Art Schatzkarte!\"";
	close;

L_Pirate:
	mes "[George der Pirat]";
	mes "\"Arrrrh! Ye du trägst einen Piratenhut!\"";
	close;

L_Bandana:
	mes "[George der Pirat]";
	mes "\"Arrrrh! Du erinnerst mich an meinen ersten Matrosen!\"";
	close;

L_EyePatch:
	mes "[George der Pirat]";
	mes "\"Arrrrh! Es sieht so aus, als hättest du ein Auge verloren!\"";
	close;
}
