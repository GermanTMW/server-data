
0103-1,95,60,0,0	monster	LavaSlime	1050,3,100000,30000,Mob0103-1::On1050
0103-1,96,60,0,0	monster	Reindeer	1057,3,100000,30000,Mob0103-1::On1057
0103-1,95,60,0,0	monster	Zentaur	1061,2,100000,30000,Mob0103-1::On1061
0103-1,95,60,0,0	monster	SantaClaus	1056,1,100000,30000,Mob0103-1::On1056
0103-1,90,66,0,0	monster	Pflanze	1030,1,100000,30000,Mob0103-1::On1030
0103-1,101,66,0,0	monster	Pflanze2	1031,1,100000,30000,Mob0103-1::On1031
0103-1,101,53,0,0	monster	Pflanze3	1032,1,100000,30000,Mob0103-1::On1032
0103-1,90,53,0,0	monster	Pflanze4	1029,1,100000,30000,Mob0103-1::On1029
0103-1,96,60,0,0	monster	Bird	1049,1,100000,30000,Mob0103-1::On1049

0103-1,0,0,0	script	Mob0103-1	NPCMINUS1,{
On1050:
	set @mobId, 1050;
	MobPoints;
	end;

On1057:
	set @mobId, 1057;
	MobPoints;
	end;

On1061:
	set @mobId, 1061;
	MobPoints;
	end;

On1056:
	set @mobId, 1056;
	MobPoints;
	end;
	
On1029:
	set @mobId, 1029;
	MobPoints;
	end;	

On1030:
	set @mobId, 1030;
	MobPoints;
	end;	

On1031:
	set @mobId, 1031;
	MobPoints;
	end;	

On1032:
	set @mobId, 1032;
	MobPoints;
	end;	

On1049:
	set @mobId, 1049;
	MobPoints;
	end;
}
